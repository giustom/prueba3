*** Settings ***
Library  SeleniumLibrary

*** Variables ***
${browser}   Chrome
${homepage}  http://automationpractice.com/index.php
${seleccion}  Other

*** Keywords ***
Select Women Option
    Click Element  //*[@id="block_top_menu"]/ul/li[1]/a
    Title Should be  Women - My Store

Select Dresses Option
    Click Element  //*[@id="block_top_menu"]/ul/li[2]/a
    Title Should be  Dresses - My Store

*** Test Cases ***
001-Caso-con-Condicionales
    Open Browser  ${homepage}  ${browser}
    Wait Until element is visible   //*[@id="header_logo"]/a/img
    Run Keyword If  '${seleccion}'=='Women'  Select Women Option  Else  Select Dresses Option
    Close Browser