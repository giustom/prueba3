*** Settings ***

Resource  recursos.robot
#Tag Suite
Force Tags  smoke

*** Test Cases ***
G001 Searching words in Google
  [Documentation]  This test case is to search words in Google
   #Tag x Test Case
  #[Tags]   regression
  #Búsquedas de palabras en google
  Open Browser and wait for Logo
  #Abrir Navegador y Esperar Logo
  Input Text  //input[@type='text']   ${WordToSearch}
  #Click Element  //img[@id='hplogo']
  #Click Element  //*[@id="tsf"]/div[2]/div/div[3]/center/input[1]
  Press Key  //*[@id="tsf"]/div[2]/div/div[3]/center/input[1]  \\13
  #Esta keyword verifica el titulo de la pagina html, para que realmente sepas cual es el titulo
  # Con el botón derecho del mouse en la pestañana "Elements", y luego Control + F , busca la palabra title
  #No estaba completo por eso te generaba error
  Title Should Be  ${WordToSearch} - Buscar con Google
  Page Should Contain  ${WordToSearch}
  Click Link  https://es.wikipedia.org/wiki/Caso_de_prueba
  #Funciono poniendo el link porque con a href no funcionaba
  #xpath://*[@id="rso"]/div[1]/div/div/div/div[1]/a/h3
  #<a href="https://es.wikipedia.org/wiki/Caso_de_prueba" ping="/url?sa=t&amp;source=web&amp;rct=j&amp;url=https://es.wikipedia.org/wiki/Caso_de_prueba&amp;ved=2ahUKEwie1dCziM7kAhWEtp4KHbHLA40QFjAAegQIAhAB"><h3 class="LC20lb"><div class="ellip">Caso de prueba - Wikipedia, la enciclopedia libre</div></h3><br><div class="TbwUpd"><cite class="iUh30 bc">https://es.wikipedia.org › wiki › Caso_de_prueba</cite></div></a>
  #<div class="ellip">Caso de prueba - Wikipedia, la enciclopedia libre</div>
  #href="https://es.wikipedia.org/wiki/Caso_de_prueba"
  Close Browser

G002 Clicking on Search button without adding any word in Google
  [Documentation]  This test case is to execute the search without adding any words
  #Hacer click en el botón de búsqueda sin escribir palabras en google
  Open Browser and wait for Logo
  #Abrir Navegador y Esperar Logo
  #Click Element  //*[@id="tsf"]/div[2]/div/div[3]/center/input[1]
  Press Key  //*[@id="tsf"]/div[2]/div/div[3]/center/input[1]  \\13
  Title Should Be  Google
  Close Browser

G003 Searching pictures in Google
  [Documentation]  This test case is to find pictures about a beach in Brasil. Exercise to use css, tags and Documentation
  [Tags]   regression
  Open Browser and wait for Logo
  Input Text  //input[@type='text']   ${PictureToSearch}
  Press Key  //*[@id="tsf"]/div[2]/div/div[3]/center/input[1]  \\13
  Click Element  css=#hdtb-msb-vis > div:nth-child(2) > a > span
  Click Image  //*[@id="sjv7Ch_DpVXypM:"]
  #To open the image I tried to use the href but I didn't find it. So, I used the xpath and worked.
  Close Browser